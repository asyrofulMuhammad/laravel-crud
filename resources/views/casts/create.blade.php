@extends('adminlte.master')
@section('content')
<div class="m-3">
  <h2>Tambah Cast</h2>
  <form action="/casts" method="POST">
      @csrf
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" name="nama" id="nama" class="form-control" value="{{ old('nama','')}}" placeholder="Masukkan Nama Pemeran">
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" name="umur" id="umur" class="form-control" value="{{ old('umur','')}}" placeholder="Masukkan Umur Pemeran">
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <input type="text" name="bio" id="bio" class="form-control" value="{{ old('bio','')}}" placeholder="Masukkan Bio Pemeran">
        @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection('content')