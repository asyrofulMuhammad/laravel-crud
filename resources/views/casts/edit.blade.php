@extends('adminlte.master')
@section('content')
<div class="m-3">
  <h2>Edit Genre {{$post->id}}</h2>
  <form action="/casts/{{$post->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" name="nama" id="nama" class="form-control" value="{{$post->nama}}" placeholder="Masukkan Nama Pemeran">
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" name="umur" id="umur" class="form-control" value="{{$post->umur}}" placeholder="Masukkan Umur Pemeran">
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <input type="text" name="bio" id="bio" class="form-control" value="{{$post->bio}}" placeholder="Masukkan bio Pemeran">
        @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection('content')