<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastsController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('casts.index', compact('casts'));
    }

    public function create() {
        return view('casts.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/casts')->with('success', 'Data Berhasil Dibuat');
    }
    

    public function show($id)
    {
        $post = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('casts')->where('id', $id)->first();
        return view('casts.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')
                    ->where('id', $id)
                    ->update([
                        "nama" => $request["nama"],
                        "umur" => $request["umur"],
                        "bio" => $request["bio"]
                    ]);
        return redirect('/casts')->with('success', 'Data Berhasil Diupdate');
    }
    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts')->with('success', 'Data Berhasil Dihapus');
    }
}
