<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Http\Request;
use DB;

class GenresController extends Controller
{
    public function create() {
        return view('genres.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:genres'
        ]);
        $query = DB::table('genres')->insert([
            "nama" => $request["nama"]
        ]);
        return redirect('/genres')->with('success', 'Data Berhasil Dibuat');
    }
    
    public function index()
    {
        $genres = DB::table('genres')->get();
        return view('genres.index', compact('genres'));
    }

    public function show($id)
    {
        $post = DB::table('genres')->where('id', $id)->first();
        return view('genres.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('genres')->where('id', $id)->first();
        return view('genres.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:genres'
        ]);
        $query = DB::table('genres')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama']
                    ]);
        return redirect('/genres')->with('success', 'Data Berhasil Diupdate');
    }
    public function destroy($id)
    {
        $query = DB::table('genres')->where('id', $id)->delete();
        return redirect('/genres')->with('success', 'Data Berhasil Dihapus');
    }
}
