<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/master', function(){
    return view('adminlte.master');
});
Route::get('/items', function(){
    return view('items.index');
});
Route::get('/items/create', function(){
    return view('items.create');
});
Route::get('/table', function(){
    return view('adminlte.table');
});
Route::get('/data-tables', function(){
    return view('adminlte.data-tables');
});
Route::get('/genres/create', 'GenresController@create');
Route::post('/genres', 'GenresController@store');
Route::get('/genres', 'GenresController@index');
Route::get('/genres/{id}', 'GenresController@show');
Route::get('/genres/{id}/edit', 'GenresController@edit');
Route::put('/genres/{id}', 'GenresController@update');
Route::delete('/genres/{id}', 'GenresController@destroy');

Route::get('/casts/create', 'CastsController@create');
Route::post('/casts', 'CastsController@store');
Route::get('/casts', 'CastsController@index');
Route::get('/casts/{id}', 'CastsController@show');
Route::get('/casts/{id}/edit', 'CastsController@edit');
Route::put('/casts/{id}', 'CastsController@update');
Route::delete('/casts/{id}', 'CastsController@destroy');